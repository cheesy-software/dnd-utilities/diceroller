#ifndef CHEETO_LIBRARIES_DICE_ROLLER_H
#define CHEETO_LIBRARIES_DICE_ROLLER_H

#include <unordered_set>
#include <random>

#include <queue> //used for testing mode, can't be wrapped in ifdef because m_rolls



#include <iostream>




class DiceRoller
{
  private:
    //type aliases
    using DistType_t = std::uniform_int_distribution<std::uint64_t>;

    //members
    std::uint64_t m_numDice = 1; //default to 1 die
    std::uint64_t m_sides = 4; //default to 4 sides on the die
    int m_modifier = 0; //default to no modifier
    bool m_exploding = false; //default to non-exploding rolls
    std::unordered_set<std::uint64_t> m_rerolls; //default to no rerolled numbers
    std::random_device m_randomDevice; //use to obtain seed
    std::mt19937 m_generator; //used to generate numbers
    std::queue<std::uint64_t> m_rolls; //used for testing mode, for some reason can't be wrapped in #ifdef
    bool m_testMode = false;

    //private functions
    void init(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode,
              const std::unordered_set<std::uint64_t>& rerolls);

  public:
    //constructors
    DiceRoller(std::uint64_t numDice, std::uint64_t sides);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, bool explode);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, const std::unordered_set<std::uint64_t>& rerolls);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier, const std::unordered_set<std::uint64_t>& rerolls);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, bool explode, const std::unordered_set<std::uint64_t>& rerolls);
    DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode, const std::unordered_set<std::uint64_t>& rerolls);
    DiceRoller(const DiceRoller& toCopy);
    DiceRoller(DiceRoller&& toMove);

    //public functions
    std::int64_t roll();
    void addReroll(std::uint64_t reroll);
    void removeReroll(std::uint64_t reroll);
    std::uint64_t getNum();

    //getters
    std::uint64_t getNumDice() const;
    std::uint64_t getSides() const;
    int getModifier() const;
    bool getExploding() const ;
    std::unordered_set<std::uint64_t> getRerolls() const;
    bool getTestMode() const;

    //setters
    void setNumDice(std::uint64_t numDice);
    void setSides(std::uint64_t sides);
    void setModifier(int modifier);
    void setExploding(bool exploding);
    void setRerolls(const std::unordered_set<std::uint64_t>& rerolls);
    void setTestMode(bool newTestMode);

    //test mode only functions
    void addRoll(std::uint64_t toAdd);
    void setRolls(std::vector<std::uint64_t> rolls);

    //operator overloads
    DiceRoller& operator=(const DiceRoller& toCopy);
    DiceRoller& operator=(DiceRoller&& toMove);
    friend bool operator==(const DiceRoller& lhs, const DiceRoller& rhs);
    friend bool operator!=(const DiceRoller& lhs, const DiceRoller& rhs);
};

#endif
