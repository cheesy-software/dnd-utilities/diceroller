#include <gtest/gtest.h>

#include "DiceRoller.h"


class FunctionalTests : public testing::Test
{
protected:
  FunctionalTests() : m_roller(4, 4, 0, false, {})
  {
  }
  virtual ~FunctionalTests()
  {
  }
  virtual void SetUp()
  {
    m_roller.setTestMode(true);
  }
  virtual void TearDown()
  {
  }
  void setupDiceRoller(std::uint64_t numDice, std::uint64_t sides, std::int64_t modifier, bool explode, std::unordered_set<std::uint64_t> rerolls)
  {
    m_roller.setNumDice(numDice);
    m_roller.setSides(sides);
    m_roller.setModifier(modifier);
    m_roller.setExploding(explode);
    m_roller.setRerolls(rerolls);
  }

  //members
  DiceRoller m_roller;
};

TEST_F(FunctionalTests, AddRerollNewEmpty)
{
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>());
  m_roller.addReroll(1);
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>({1}));
}
TEST_F(FunctionalTests, AddRerollNewNotEmpty)
{
  m_roller.setRerolls({1, 2});
  m_roller.addReroll(3);
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>({1, 2, 3}));
}
TEST_F(FunctionalTests, AddRerollOld)
{
  m_roller.setRerolls({1, 2});
  m_roller.addReroll(2);
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>({1, 2}));
}
TEST_F(FunctionalTests, AddRerollInvalid)
{
  m_roller.setRerolls({1, 2});
  m_roller.addReroll(6);
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>({1, 2}));
}
TEST_F(FunctionalTests, RemoveRerollExists)
{
  m_roller.setRerolls({1, 2});
  m_roller.removeReroll(2);
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>({1}));
}
TEST_F(FunctionalTests, RemoveRerollNonExists)
{
  m_roller.setRerolls({1, 2,});
  EXPECT_NO_THROW(m_roller.removeReroll(3));
  EXPECT_EQ(m_roller.getRerolls(), std::unordered_set<std::uint64_t>({1, 2}));
}
/* normal roll
 *   have a couple functions that roll different amounts of numbers
 *     different numbers of dice with different sides
 *       4d4
 *       4d8
 *       8d6
 *       12d7
 */
TEST_F(FunctionalTests, Normal4_4)
{
  setupDiceRoller(4, 4, 0, false, {});
  m_roller.setRolls({3, 1, 4, 3, 1, 4, 2, 3});
  EXPECT_EQ(m_roller.roll(), 11);
}
TEST_F(FunctionalTests, Normal4_8)
{
  setupDiceRoller(4, 8, 0, false, {});
  m_roller.setRolls({7, 2, 5, 3, 1, 6, 2, 8});
  EXPECT_EQ(m_roller.roll(), 17);
}
TEST_F(FunctionalTests, Normal8_6)
{
  setupDiceRoller(8, 6, 0, false, {});
  m_roller.setRolls({1, 5, 6, 4, 2, 3, 2, 2, 6, 6, 3, 6});
  EXPECT_EQ(m_roller.roll(), 25);
}
TEST_F(FunctionalTests, Normal12_7)
{
  setupDiceRoller(12, 7, 0, false, {});
  m_roller.setRolls({2, 5, 7, 5, 1, 3, 2, 3, 2, 6, 5, 1, 4, 6, 4, 4});
  EXPECT_EQ(m_roller.roll(), 42);
}

//MODIFIER TESTS
/* roll with modifier
 *   have a couple functions that roll different amounts of numbers
 *     different numbers of dice with different sides
 *       4d4 + 4
 *       4d8 + 19
 *       8d6 - 3
 *       12d7 - 293
 */
TEST_F(FunctionalTests, Modifier4_4_4)
{
  setupDiceRoller(4, 4, 4, false, {});
  m_roller.setRolls({3, 1, 4, 3, 1, 4, 2, 3});
  EXPECT_EQ(m_roller.roll(), 15);
}
TEST_F(FunctionalTests, Modifier4_8_19)
{
  setupDiceRoller(4, 8, 19, false, {});
  m_roller.setRolls({7, 2, 5, 3, 1, 6, 2, 8});
  EXPECT_EQ(m_roller.roll(), 36);
}
TEST_F(FunctionalTests, Modifier8_6_Negative3)
{
  setupDiceRoller(8, 6, -3, false, {});
  m_roller.setRolls({1, 5, 6, 4, 2, 3, 2, 2, 6, 6, 3, 6});
  EXPECT_EQ(m_roller.roll(), 22);
}
TEST_F(FunctionalTests, Modifier12_7_Negative293)
{
  setupDiceRoller(12, 7, -293, false, {});
  m_roller.setRolls({2, 5, 7, 5, 1, 3, 2, 3, 2, 6, 5, 1, 4, 6, 4, 4});
  EXPECT_EQ(m_roller.roll(), -251);
}

//EXPLODING TESTS
/* roll with exploding
 *   Normal version that has a couple explodes throught the generated numbers
 *     different numbers of dice with different sides
 *       4d4 - 2 explodes
 *       4d8 - 0 explode
 *       8d6 - 4 explodes (2 in a row)
 *       12d7 - 8 explodes (3 in a row)
 *     Edge cases
 *       explode as the first roll (covered by several other tests)
 *       explode 8 times in a row
 *       explode a couple times, then explode after the normal roll count would've been reached
 */
TEST_F(FunctionalTests, Exploding4_4_Twice)
{
  setupDiceRoller(4, 4, 0, true, {});
  m_roller.setRolls({4, 1, 4, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 16);
}
TEST_F(FunctionalTests, Exploding4_8_Never)
{
  setupDiceRoller(4, 8, 0, true, {});
  m_roller.setRolls({7, 2, 5, 3, 1, 6, 2, 8});
  EXPECT_EQ(m_roller.roll(), 17);
}
TEST_F(FunctionalTests, Exploding8_6_Four_Twice_In_Row)
{
  setupDiceRoller(8, 6, 0, true, {});
  m_roller.setRolls({6, 5, 6, 4, 6, 6, 2, 2, 1, 5, 3, 2, 4, 2, 5, 6});
  EXPECT_EQ(m_roller.roll(), 48);
}
TEST_F(FunctionalTests, Exploding12_7_Eight_Thrice_In_Row)
{
  setupDiceRoller(12, 7, 0, true, {});
  m_roller.setRolls({7, 7, 7, 5, 7, 3, 2, 6, 2, 7, 5, 7, 4, 6, 4, 4, 1, 6, 4, 3});
  EXPECT_EQ(m_roller.roll(), 90);
}
TEST_F(FunctionalTests, ExplodingExplodeEightInRow)
{
  setupDiceRoller(12, 7, 0, true, {});
  m_roller.setRolls({7, 7, 7, 7, 7, 7, 7, 7, 2, 2, 5, 3, 4, 6, 4, 4, 1, 6, 4, 3, 2});
  EXPECT_EQ(m_roller.roll(), 100);
}
TEST_F(FunctionalTests, ExplodingExplodeThenExplodeAgain)
{
  setupDiceRoller(8, 6, 0, true, {});
  m_roller.setRolls({6, 5, 6, 4, 6, 6, 2, 2, 6, 5, 3, 2, 6, 2, 5, 6});
  EXPECT_EQ(m_roller.roll(), 61);
}

/* roll with rerolls
 *   Normal version
 *     different numbers of dice with different sides
 *       4d4 - 2 rerolls
 *       4d8 - 0 rerolls
 *       8d6 - 4 rerolls (2 in a row)
 *       12d7 - 8 rerolls (3 in a row)
 *     Edge cases
 *       reroll as the first roll (covered by other tests)
 *       reroll 8 times in a row
 *       reroll then reroll outside the normal dice amount (ex, reroll 2nd then 5th die rolled if there are only 4 numbers to roll)
 *       reroll with multiple different numbers as reroll options
 */
TEST_F(FunctionalTests, Reroll4_4_2)
{
  setupDiceRoller(4, 4, 0, false, {4});
  m_roller.setRolls({4, 1, 4, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 8);
}
TEST_F(FunctionalTests, Reroll4_8_0)
{
  setupDiceRoller(4, 8, 0, false, {4});
  m_roller.setRolls({7, 2, 5, 3, 4, 6, 2, 8});
  EXPECT_EQ(m_roller.roll(), 17);
}
TEST_F(FunctionalTests, Reroll8_6_4_2_In_Row)
{
  setupDiceRoller(8, 6, 0, false, {6});
  m_roller.setRolls({6, 5, 6, 4, 6, 6, 2, 2, 1, 5, 3, 2, 4, 2, 5, 6});
  EXPECT_EQ(m_roller.roll(), 24);
}
TEST_F(FunctionalTests, Reroll12_7_8_3_In_Row)
{
  setupDiceRoller(12, 7, 0, false, {7});
  m_roller.setRolls({7, 7, 7, 5, 7, 3, 2, 6, 2, 7, 5, 7, 4, 6, 4, 4, 1, 6, 4, 3});
  EXPECT_EQ(m_roller.roll(), 48);
}
TEST_F(FunctionalTests, RerollEightInRow)
{
  setupDiceRoller(12, 7, 0, false, {7});
  m_roller.setRolls({7, 7, 7, 7, 7, 7, 7, 7, 2, 2, 5, 3, 4, 6, 4, 4, 1, 6, 4, 3, 2});
  EXPECT_EQ(m_roller.roll(), 44);
}
TEST_F(FunctionalTests, RerollThenRerollOutsideDiceCount)
{
  setupDiceRoller(4, 4, 0, false, {2});
  m_roller.setRolls({3, 2, 1, 4, 2, 1, 4});
  EXPECT_EQ(m_roller.roll(), 9);
}
TEST_F(FunctionalTests, RerollMultipleNumbers)
{
  setupDiceRoller(4, 4, 0, false, {2, 3});
  m_roller.setRolls({3, 2, 1, 4, 2, 1, 4, 1, 2, 4, 3});
  EXPECT_EQ(m_roller.roll(), 10);
}

TEST_F(FunctionalTests, ModifierExplode)
{
  setupDiceRoller(4, 4, 3, true, {});
  m_roller.setRolls({4, 1, 4, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 19);
}
TEST_F(FunctionalTests, NegativeModifierExplode)
{
  setupDiceRoller(4, 4, -3, true, {});
  m_roller.setRolls({4, 1, 4, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 13);
}
TEST_F(FunctionalTests, ModifierExplodeExplodeAgain)
{
  setupDiceRoller(8, 6, 14, true, {});
  m_roller.setRolls({6, 5, 6, 4, 6, 6, 2, 2, 6, 5, 3, 2, 6, 2, 5, 6});
  EXPECT_EQ(m_roller.roll(), 75);
}
TEST_F(FunctionalTests, NegativeModifierExplodeExplodeAgain)
{
  setupDiceRoller(8, 6, -14, true, {});
  m_roller.setRolls({6, 5, 6, 4, 6, 6, 2, 2, 6, 5, 3, 2, 6, 2, 5, 6});
  EXPECT_EQ(m_roller.roll(), 47);
}

TEST_F(FunctionalTests, ModifierReroll)
{
  setupDiceRoller(4, 4, 3, false, {4});
  m_roller.setRolls({4, 1, 4, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 11);
}
TEST_F(FunctionalTests, NegativeModifierReroll)
{
  setupDiceRoller(4, 4, -3, false, {4});
  m_roller.setRolls({4, 1, 4, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 5);
}

TEST_F(FunctionalTests, ExplodingReroll)
{
  setupDiceRoller(4, 8, 0, true, {3});
  m_roller.setRolls({8, 1, 2, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 14);
}
TEST_F(FunctionalTests, ExplodingRerollRerollIsExplode)
{
  setupDiceRoller(4, 8, 0, true, {3});
  m_roller.setRolls({8, 1, 2, 3, 8, 3, 2, 4, 5});
  EXPECT_EQ(m_roller.roll(), 25);
}
TEST_F(FunctionalTests, ExplodingRerollExplodeIsReroll)
{
  setupDiceRoller(4, 8, 0, true, {3});
  m_roller.setRolls({8, 3, 2, 7, 5, 7, 2, 4, 5});
  EXPECT_EQ(m_roller.roll(), 29);
}

TEST_F(FunctionalTests, ModifierExplodingReroll)
{
  setupDiceRoller(4, 8, 3, true, {3});
  m_roller.setRolls({8, 1, 2, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 17);
}
TEST_F(FunctionalTests, NegativeModifierExplodingReroll)
{
  setupDiceRoller(4, 8, -3, true, {3});
  m_roller.setRolls({8, 1, 2, 3, 1, 3, 2, 4});
  EXPECT_EQ(m_roller.roll(), 11);
}

