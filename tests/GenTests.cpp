#include <unordered_map>

#include <gtest/gtest.h>

#include "DiceRoller.h"


class GenTests : public testing::Test
{
protected:
  GenTests() : m_roller(4, 4)
  {
  }
  virtual ~GenTests()
  {
  }
  virtual void SetUp()
  {
  }
  virtual void TearDown()
  {
  }
  void runTests(std::uint64_t sides, std::uint64_t numToRoll)
  {
    //setup roller's number of sides
    m_roller.setSides(sides);

    //generate a frequency map for the rolls
    std::unordered_map<std::uint64_t, std::uint64_t> freqMap;
    for (std::uint64_t i = 0; i < numToRoll; i++)
    {
      auto res = m_roller.getNum();
      if (freqMap.count(res) == 0)
      {
        freqMap[res] = 0;
      } //end  if (freqMap.count(res) == 0)
      else
      {
        freqMap[res]++;
      } //end  else
    } //end  for (std::uint64_t i = 0; i < numToRoll; i++)

    //ensure that no numbers over max were returned
    ASSERT_EQ(freqMap.size(), sides);
    for (std::uint64_t i = 1; i <= sides; i++)
    {
      EXPECT_EQ(freqMap.count(i), 1);
    } //end  for (std::uint64_t i = 1; i <= sides; i++)

    //verify a decent distribution of numbers
    const auto expectedNum = (double)numToRoll / (double)sides;
    const auto expectedPercent = expectedNum / (double)numToRoll;
    for (std::uint64_t i = 1; i <= sides; i++)
    {
      auto percent = (double)freqMap[i] / (double)numToRoll;
      auto percentDiff = std::abs(percent - expectedPercent);
      EXPECT_LT(percentDiff, 1E-3);
    } //end  for (std::uint64_t i = 1; i <= sides; i++)
  }

  //members
  DiceRoller m_roller;
};

TEST_F(GenTests, getNum4)
{
  runTests(4, 5000000);
}
TEST_F(GenTests, getNum8)
{
  runTests(8, 5000000);
}
TEST_F(GenTests, getNum192)
{
  runTests(192, 5000000);
}

