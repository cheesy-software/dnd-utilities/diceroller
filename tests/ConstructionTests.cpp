#include <gtest/gtest.h>

#include <memory>

#include "DiceRoller.h"

class ConstructionTests : public testing::Test
{
protected:
  ConstructionTests()
  {
  }
  virtual ~ConstructionTests()
  {
  }
  virtual void SetUp()
  {
  }
  virtual void TearDown()
  {
  }
  void checkValues(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode, std::unordered_set<std::uint64_t> rerolls)
  {
    EXPECT_EQ(m_diceRoller->getNumDice(), numDice);
    EXPECT_EQ(m_diceRoller->getSides(), sides);
    EXPECT_EQ(m_diceRoller->getModifier(), modifier);
    EXPECT_EQ(m_diceRoller->getExploding(), explode);
    EXPECT_EQ(m_diceRoller->getRerolls(), rerolls);
  }
  std::unique_ptr<DiceRoller> m_diceRoller;
};

TEST_F(ConstructionTests, DiceSides)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4);
  checkValues(4, 4, 0, false, {});
}
TEST_F(ConstructionTests, DiceSidesModifier)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, -4);
  checkValues(4, 4, -4, false, {});
}
TEST_F(ConstructionTests, DiceSidesExplode)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, true);
  checkValues(4, 4, 0, true, {});
}
TEST_F(ConstructionTests, DiceSidesRerolls)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, std::unordered_set<std::uint64_t>({1, 3, 5}));
  checkValues(4, 4, 0, false, {1, 3});
}
TEST_F(ConstructionTests, DiceSidesModifierExplode)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, -295, true);
  checkValues(4, 4, -295, true, {});
}
TEST_F(ConstructionTests, DiceSidesModifierReroll)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, 34, std::unordered_set<std::uint64_t>({1, 5, 29}));
  checkValues(4, 4, 34, false, {1});
}
TEST_F(ConstructionTests, DiceSidesExplodeReroll)
{
  m_diceRoller = std::make_unique<DiceRoller>(39, 3, true, std::unordered_set<std::uint64_t>({2, 4}));
  checkValues(39, 3, 0, true, {2});
}
TEST_F(ConstructionTests, DiceSidesModifierExplodeReroll)
{
  m_diceRoller = std::make_unique<DiceRoller>(13, 294, 321, true, std::unordered_set<std::uint64_t>({3, 3, 4, 2}));
  checkValues(13, 294, 321, true, {3, 3, 4, 2});
}
TEST_F(ConstructionTests, RerollInvalidHigherThanMax)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, 0, false, std::unordered_set<std::uint64_t>({2, 6}));
  checkValues(4, 4, 0, false, {2});
}
TEST_F(ConstructionTests, RerollValidMaxNoExplode)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, 0, false, std::unordered_set<std::uint64_t>({2, 4}));
  checkValues(4, 4, 0, false, {2, 4});
}
TEST_F(ConstructionTests, RerollInvalidMaxExplode)
{
  m_diceRoller = std::make_unique<DiceRoller>(4, 4, 0, true, std::unordered_set<std::uint64_t>({2, 4}));
  checkValues(4, 4, 0, true, {2});
}
TEST_F(ConstructionTests, CopyConstruction)
{
  DiceRoller roller(2, 48, 3, true, {1, 2, 4, 5});
  m_diceRoller = std::make_unique<DiceRoller>(roller);
  checkValues(2, 48, 3, true, {1, 2, 4, 5});
}
TEST_F(ConstructionTests, MoveConstruction)
{
  DiceRoller roller(2, 48, 3, true, {1, 2, 4, 5});
  m_diceRoller = std::make_unique<DiceRoller>(std::move(roller));
  checkValues(2, 48, 3, true, {1, 2, 4, 5});
}

