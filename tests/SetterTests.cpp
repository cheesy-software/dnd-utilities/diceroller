#include <gtest/gtest.h>

#include "DiceRoller.h"

class SetterTests : public testing::Test
{
protected:
  SetterTests() : m_diceRoller(4, 4, 3, false, std::unordered_set<std::uint64_t>({1, 2, 3}))
  {
  }
  virtual ~SetterTests()
  {
  }
  virtual void SetUp()
  {
  }
  virtual void TearDown()
  {
  }

  //members
  DiceRoller m_diceRoller;
};

TEST_F(SetterTests, SetNumDiceValid)
{
  m_diceRoller.setNumDice(9);
  EXPECT_EQ(m_diceRoller.getNumDice(), 9);
}
TEST_F(SetterTests, SetNumDiceInvalid)
{
  m_diceRoller.setNumDice(0); //can't have 0 dice
  EXPECT_EQ(m_diceRoller.getNumDice(), 4); //shouldn't change on invalid input
}
TEST_F(SetterTests, SetSidesValid)
{
  m_diceRoller.setSides(9);
  EXPECT_EQ(m_diceRoller.getSides(), 9);
}
TEST_F(SetterTests, SetSidesInvalid)
{
  m_diceRoller.setSides(0); //can't have 0 sides
  EXPECT_EQ(m_diceRoller.getSides(), 4); //shouldn't change on invalid input
}
TEST_F(SetterTests, SetModifierValid)
{
  m_diceRoller.setModifier(-13);
  EXPECT_EQ(m_diceRoller.getModifier(), -13);
}
TEST_F(SetterTests, SetExplodingValid)
{
  m_diceRoller.setExploding(true);
  EXPECT_EQ(m_diceRoller.getExploding(), true);
}
TEST_F(SetterTests, SetRerollsValid)
{
  m_diceRoller.setRerolls({4, 2});
  EXPECT_EQ(m_diceRoller.getRerolls(), std::unordered_set<std::uint64_t>({4, 2}));
}
TEST_F(SetterTests, SetRerollsInvalid)
{
  m_diceRoller.setRerolls({4, 2, 6, 19});
  EXPECT_EQ(m_diceRoller.getRerolls(), std::unordered_set<std::uint64_t>({4, 2}));
}

