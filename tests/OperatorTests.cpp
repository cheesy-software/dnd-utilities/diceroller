#include <gtest/gtest.h>

#include "DiceRoller.h"

class OperatorTests : public testing::Test
{
protected:
  OperatorTests(): m_defaultRoller(3, 5, 2, true, {1, 5, 2, 9})
  {
  }
  virtual ~OperatorTests()
  {
  }
  virtual void SetUp()
  {
  }
  virtual void TearDown()
  {
  }
  void checkValues(DiceRoller toCheck, std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode, std::unordered_set<std::uint64_t> rerolls)
  {
    EXPECT_EQ(toCheck.getNumDice(), numDice);
    EXPECT_EQ(toCheck.getSides(), sides);
    EXPECT_EQ(toCheck.getModifier(), modifier);
    EXPECT_EQ(toCheck.getExploding(), explode);
    EXPECT_EQ(toCheck.getRerolls(), rerolls);
  }

  //members
  DiceRoller m_defaultRoller;
};

TEST_F(OperatorTests, CopyAssignment)
{
  DiceRoller roller(4, 4);
  checkValues(roller, 4, 4, 0, false, std::unordered_set<std::uint64_t>());
  roller = m_defaultRoller;
  checkValues(roller, m_defaultRoller.getNumDice(), m_defaultRoller.getSides(),
                      m_defaultRoller.getModifier(), m_defaultRoller.getExploding(),
                      m_defaultRoller.getRerolls());
}
TEST_F(OperatorTests, MoveAssignment)
{
  DiceRoller roller(4, 4);
  checkValues(roller, 4, 4, 0, false, std::unordered_set<std::uint64_t>());
  roller = std::move(m_defaultRoller);
  checkValues(roller, m_defaultRoller.getNumDice(), m_defaultRoller.getSides(),
                      m_defaultRoller.getModifier(), m_defaultRoller.getExploding(),
                      m_defaultRoller.getRerolls());
}
TEST_F(OperatorTests, EqualityOperator)
{
  DiceRoller roller(3, 5, 2, true, {1, 5, 2, 9});
  EXPECT_TRUE(roller == m_defaultRoller);
  EXPECT_FALSE(roller != m_defaultRoller);
}
TEST_F(OperatorTests, InequalityOperator)
{
  //-------------------------------------------
  //Single Value Differences
  //-------------------------------------------
  //num dice
  DiceRoller roller(1, 5, 2, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides
  roller = DiceRoller(3, 1, 2, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //modifier
  roller = DiceRoller(3, 5, 1, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //exploding
  roller = DiceRoller(3, 5, 2, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //rerolls
  roller = DiceRoller(3, 5, 2, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);

  //-------------------------------------------
  //Dual Value Differences
  //-------------------------------------------
  //num dice + sides
  roller = DiceRoller(1, 1, 2, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + modifier
  roller = DiceRoller(1, 5, 1, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + exploding
  roller = DiceRoller(1, 5, 2, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + rerolls
  roller = DiceRoller(1, 5, 2, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + modifier
  roller = DiceRoller(3, 1, 1, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + exploding
  roller = DiceRoller(3, 1, 2, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + rerolls
  roller = DiceRoller(3, 1, 2, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //modifier + exploding
  roller = DiceRoller(3, 5, 1, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //modifier + rerolls
  roller = DiceRoller(3, 5, 1, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //exploding + rerolls
  roller = DiceRoller(3, 5, 2, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);

  //-------------------------------------------
  //Triple Value Differences
  //-------------------------------------------
  //num dice + sides + modifier
  roller = DiceRoller(1, 1, 1, true, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + sides + exploding
  roller = DiceRoller(1, 1, 2, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + sides + rerolls
  roller = DiceRoller(1, 1, 2, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + modifier + exploding
  roller = DiceRoller(1, 5, 1, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + modifier + rerolls
  roller = DiceRoller(1, 5, 1, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + exploding + rerolls
  roller = DiceRoller(1, 5, 2, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + modifier + exploding
  roller = DiceRoller(3, 1, 1, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + modifier + rerolls
  roller = DiceRoller(3, 1, 1, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + exploding + rerolls
  roller = DiceRoller(3, 1, 2, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //modifier + exploding + rerolls
  roller = DiceRoller(3, 5, 1, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);

  //-------------------------------------------
  //Quadruple Value Differences
  //-------------------------------------------
  //num dice + sides + modifier + exploding
  roller = DiceRoller(1, 1, 1, false, {1, 5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + sides + modifier + rerolls
  roller = DiceRoller(1, 1, 1, true, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + sides + exploding + rerolls
  roller = DiceRoller(1, 1, 2, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //num dice + modifier + exploding + rerolls
  roller = DiceRoller(1, 5, 1, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
  //sides + modifier + exploding + rerolls
  roller = DiceRoller(3, 1, 1, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);

  //-------------------------------------------
  //Quintuple Value Differences
  //-------------------------------------------
  //num dice + sides + modifier + exploding + rerolls
  roller = DiceRoller(1, 1, 1, false, {5, 2, 9});
  EXPECT_FALSE(roller == m_defaultRoller);
  EXPECT_TRUE(roller != m_defaultRoller);
}

