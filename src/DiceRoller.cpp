#include "DiceRoller.h"

#include <iostream> //TEMP

//private functions
void DiceRoller::init(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode, const std::unordered_set<std::uint64_t>& rerolls)
{
  //set the member variables
  setNumDice(numDice);
  setSides(sides);
  setModifier(modifier);
  setExploding(explode);
  setRerolls(rerolls);

  //set the random number generation values
  m_generator = std::mt19937(m_randomDevice());
}

//constructors
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides)
{
  init(numDice, sides, 0, false, {});
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier)
{
  init(numDice, sides, modifier, false, {});
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, bool explode)
{
  init(numDice, sides, 0, explode, {});
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, const std::unordered_set<std::uint64_t>& rerolls)
{
  init(numDice, sides, 0, false, rerolls);
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode)
{
  init(numDice, sides, modifier, explode, {});
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier, const std::unordered_set<std::uint64_t>& rerolls)
{
  init(numDice, sides, modifier, false, rerolls);
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, bool explode, const std::unordered_set<std::uint64_t>& rerolls)
{
  init(numDice, sides, 0, explode, rerolls);
}
DiceRoller::DiceRoller(std::uint64_t numDice, std::uint64_t sides, int modifier, bool explode, const std::unordered_set<std::uint64_t>& rerolls)
{
  init(numDice, sides, modifier, explode, rerolls);
}
DiceRoller::DiceRoller(const DiceRoller& toCopy)
{
  init(toCopy.getNumDice(), toCopy.getSides(), toCopy.getModifier(), toCopy.getExploding(), toCopy.getRerolls());
}
DiceRoller::DiceRoller(DiceRoller&& toMove)
{
  init(toMove.getNumDice(), toMove.getSides(), toMove.getModifier(), toMove.getExploding(), toMove.getRerolls());
}


//public functions
std::int64_t DiceRoller::roll()
{
  std::int64_t total = 0;
  //generate a number for each of the dice being rolled
  auto diceToRoll = m_numDice;
  for (std::uint64_t i = 0; i < diceToRoll; i++)
  {
    std::uint64_t roll = 0; //start at an impossible roll
    do
    {
      roll = getNum();
    } while (m_rerolls.count(roll) != 0);

    //add roll to the total
    total += roll;

    //check if exploading is needed
    if (m_exploding && roll == m_sides)
    {
      //if exploading, make it so another die needs rolled
      diceToRoll++;
    } //end  if (m_exploding && roll == m_sides)
  } //end  for (size_t i = 0; i < m_numDice; i++)

  //add the modifier in
  return total + m_modifier;
}
void DiceRoller::addReroll(std::uint64_t reroll)
{
  if ((m_exploding ? (reroll < m_sides) : (reroll <= m_sides)))
  {
    m_rerolls.insert(reroll);
  } //end  if ((m_exploding ? (reroll < m_sides) : (reroll <= m_sides)))
}
void DiceRoller::removeReroll(std::uint64_t reroll)
{
  m_rerolls.erase(reroll);
}

//getters
std::uint64_t DiceRoller::getNumDice() const
{
  return m_numDice;
}
std::uint64_t DiceRoller::getSides() const
{
  return m_sides;
}
int DiceRoller::getModifier() const
{
  return m_modifier;
}
bool DiceRoller::getExploding() const
{
  return m_exploding;
}
std::unordered_set<std::uint64_t> DiceRoller::getRerolls() const
{
  return m_rerolls;
}
bool DiceRoller::getTestMode() const
{
  return m_testMode;
}

//setters
void DiceRoller::setNumDice(std::uint64_t numDice)
{
  if (numDice > 0)
  {
    m_numDice = numDice;
  }
}
void DiceRoller::setSides(std::uint64_t sides)
{
  //dice can't have less than 2 sided
  if (sides > 1)
  {
    m_sides = sides;
  } //end  if (sides > 1)
}
void DiceRoller::setModifier(int modifier)
{
  m_modifier = modifier;
}
void DiceRoller::setExploding(bool exploding)
{
  m_exploding = exploding;
}
void DiceRoller::setRerolls(const std::unordered_set<std::uint64_t>& rerolls)
{
  m_rerolls.clear();
  for (const auto& reroll : rerolls)
  {
    addReroll(reroll);
  } //end  for (const auto& reroll : rerolls)
}
void DiceRoller::setTestMode(bool testMode)
{
  m_testMode = testMode;
}

//test mode only functions
void DiceRoller::addRoll(std::uint64_t toAdd)
{
  m_rolls.push(toAdd);
}
void DiceRoller::setRolls(std::vector<std::uint64_t> rolls)
{
  std::queue<std::uint64_t> empty;
  std::swap(m_rolls, empty);
  for (auto val : rolls)
  {
    m_rolls.push(val);
  }
}
std::uint64_t DiceRoller::getNum()
{
  if (m_testMode)
  {
    //std::cout << "inside not random rolls" << std::endl;
    auto roll = m_rolls.front();
    m_rolls.pop();
    return roll;
  }
  else
  {
    //std::cout << "inside random rolls" << std::endl;
    return DistType_t(1, m_sides)(m_generator);
  }
}

//operator overloads
DiceRoller& DiceRoller::operator=(const DiceRoller& toCopy)
{
  this->setNumDice(toCopy.getNumDice());
  this->setSides(toCopy.getSides());
  this->setModifier(toCopy.getModifier());
  this->setExploding(toCopy.getExploding());
  this->setRerolls(toCopy.getRerolls());

  return *this;
}
DiceRoller& DiceRoller::operator=(DiceRoller&& toMove)
{
  this->setNumDice(toMove.getNumDice());
  this->setSides(toMove.getSides());
  this->setModifier(toMove.getModifier());
  this->setExploding(toMove.getExploding());
  this->setRerolls(toMove.getRerolls());

  return *this;
}
bool operator==(const DiceRoller& lhs, const DiceRoller& rhs)
{
  return lhs.m_numDice == rhs.m_numDice &&
         lhs.m_sides == rhs.m_sides &&
         lhs.m_modifier == rhs.m_modifier &&
         lhs.m_exploding == rhs.m_exploding &&
         lhs.m_rerolls == rhs.m_rerolls;
}
bool operator!=(const DiceRoller& lhs, const DiceRoller& rhs)
{
  return !(lhs == rhs);
}
